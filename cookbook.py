import json
#functions for json file, not use in this version
# def import_from_json(file_name='products.json'):
#     with open(file_name, encoding='utf-8') as data_file:
#         data = json.load(data_file)
#         pprint(data)
#
# def export_to_json(cook_book, file_name='export.json'):
#     with open(file_name, 'w', encoding='utf-8') as data_file:
#         data = json.dumps(cook_book, indent=2, sort_keys=True,
#                             separators=(',', ':'), ensure_ascii=False)
#         data_file.write(data)

def load_from_file(file_name='products.txt'):
    cook_book = {}
    with open(file_name, encoding='utf-8') as data_file:
        for line in data_file:
            dish_name = str(line).strip().lower()
            cook_book[dish_name]=[]
            line = data_file.readline().strip()
            for i in range(int(line)):
                line = data_file.readline().strip()
                name, quantity, measure = line.split(' | ')
                ingridients_dict = {'ingridient_name': name, 'quantity': int(quantity), 'measure': measure}
                cook_book[dish_name].append(ingridients_dict)
    #     for line in data_file:
    #         line = line.strip().lower()
    #         try:
    #             #parse ingridients from file and append in cook_boock dict
    #             name, quantity, measure = line.split(' | ')
    #             ingridients_dict = {'ingridient_name': name, 'quantity': int(quantity), 'measure': measure}
    #             cook_book[dish_name].append(ingridients_dict)
    #         except ValueError:
    #             #if not ingridients then check for dish name or ingridients count and parse them
    #             try:
    #                 #get number of ingridients, not use in this code
    #                 _ = int(line)
    #             except ValueError:
    #                 #this line in cook book file contains dish name, then create a dict element with empty list of ingridients
    #                 dish_name = str(line)
    #                 cook_book[dish_name] = []
    # export_to_json(cook_book)
    return cook_book

def get_shop_list_by_dishes(dishes, person_count):
  shop_list = {}
  cook_book = load_from_file()
  for dish in dishes:
    for ingridient in cook_book[dish]:
      new_shop_list_item = dict(ingridient)
      new_shop_list_item['quantity'] *= person_count
      if new_shop_list_item['ingridient_name'] not in shop_list:
        shop_list[new_shop_list_item['ingridient_name']] = new_shop_list_item
      else:
        shop_list[new_shop_list_item['ingridient_name']]['quantity'] += new_shop_list_item['quantity']
  return shop_list

def print_shop_list(shop_list):

  for shop_list_item in shop_list.values():
    print('{ingridient_name} {quantity} {measure}'.format(**shop_list_item))

def create_shop_list():
  person_count = int(input('Введите количество человек: '))
  dishes = input('Введите блюда в расчете на одного человека (через запятую): ').lower().split(', ')
  shop_list = get_shop_list_by_dishes(dishes, person_count)
  print_shop_list(shop_list)

create_shop_list()
